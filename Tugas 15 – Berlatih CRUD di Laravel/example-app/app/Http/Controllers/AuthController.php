<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showRegistrationForm()
    {
        return view('register');
    }

    public function send(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        
        $nama = $fname . ' ' . $lname;
    
        return view('welcome', ['nama' => $nama]);
    }
    
    
    
}
