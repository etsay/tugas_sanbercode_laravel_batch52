<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Cast extends Model
{
    protected $fillable = ['nama', 'bio', 'umur'];

    protected $attributes = [
        'umur' => 0, // Set a default value (0 in this case)
    ];
}
