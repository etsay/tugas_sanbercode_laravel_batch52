<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
// File: database/migrations/YYYY_MM_DD_create_casts_table.php

public function up()
{
    Schema::create('casts', function (Blueprint $table) {
        $table->id();
        $table->string('nama');
        $table->text('bio');
        $table->integer('umur')->nullable(); // Allow NULL values
        $table->timestamps();
    });
}


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cast');
    }
};
