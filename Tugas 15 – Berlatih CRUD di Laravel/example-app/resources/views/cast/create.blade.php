@extends('layout.master')
@section('judul')
Halaman Create Cast
@endsection

@section('konten')
    <h1>Formulir Tambah Pemain Film Baru</h1>

    <form action="{{ route('cast.store') }}" method="POST">
        @csrf
        <div class="mb-3">
            <label for="nama" class="form-label">Nama Pemain:</label>
            <input type="text" class="form-control" name="nama" required>
        </div>
        <div class="mb-3">
            <label for="bio" class="form-label">Deskripsi:</label>
            <textarea class="form-control" name="bio" required></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection