@extends('layout.master')

@section('judul')
    Detail Pemain Film
@endsection

@section('konten')
    <h1>Detail Pemain Film</h1>

    <div>
        <strong>Nama Pemain:</strong>
        <p>{{ $cast->nama }}</p>
    </div>

    <div>
        <strong>Deskripsi:</strong>
        <p>{{ $cast->bio }}</p>
    </div>

    <a href="{{ route('cast.edit', $cast->id) }}" class="btn btn-warning">Edit</a>

    <form action="{{ route('cast.destroy', $cast->id) }}" method="POST" style="display: inline;">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Hapus</button>
    </form>
@endsection