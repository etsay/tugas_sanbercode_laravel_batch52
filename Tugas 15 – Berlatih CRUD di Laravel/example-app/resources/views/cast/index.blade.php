@extends('layout.master')
@section('judul')
Daftar Pemain Film
@endsection

@section('konten')
    <h1>List Data Para Pemain Film</h1>

    <a href="{{ route('cast.create') }}" class="btn btn-primary mb-2">Tambah Pemain</a>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Nama</th>
                <th scope="col">Bio</th>
                <th scope="col">Umur</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($casts as $cast)
                <tr>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>
                        <a href="{{ route('cast.show', $cast->id) }}" class="btn btn-info btn-sm">Detail</a>
                        <a href="{{ route('cast.edit', $cast->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <form action="{{ route('cast.destroy', $cast->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
