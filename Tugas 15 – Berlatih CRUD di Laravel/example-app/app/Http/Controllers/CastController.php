<?php
namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('cast.index', compact('casts'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
        ]);

        Cast::create([
            'nama' => $request->input('nama'),
            'bio' => $request->input('bio'),
        ]);

        return redirect()->route('cast.index');
    }

    public function show($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
        ]);

        $cast = Cast::findOrFail($cast_id);
        $cast->update([
            'nama' => $request->input('nama'),
            'bio' => $request->input('bio'),
        ]);

        return redirect()->route('cast.show', $cast_id);
    }

    public function destroy($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);
        $cast->delete();

        return redirect()->route('cast.index');
    }
}
