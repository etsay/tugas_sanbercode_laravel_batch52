<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/send" method="post">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" name="fname" id="fname"><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" name="lname" id="lname"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" value="Male" id="gender">Male<br>
        <input type="radio" name="gender" value="Female" id="gender">Female<br>
        <input type="radio" name="gender" value="Other" id="gender">Other<br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Rusia">Rusia</option>
            <option value="China">China</option>
            <option value="Korea">Korea</option>
        </select><br><br>
        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox" name="language[]" value="Bahasa Indonesia" id="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language[]" value="English" id="language">English<br>
        <input type="checkbox" name="language[]" value="Other" id="language">Other<br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="send">
    </form>
</body>
</html>
