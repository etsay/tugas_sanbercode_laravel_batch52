@extends('layout.master')
@section('judul')
Edit Pemain Film
@endsection

@section('konten')
    <h1>Edit Data Pemain Film</h1>

    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
        @csrf
        @method('PUT')

        <label for="nama">Nama Pemain:</label>
        <input type="text" name="nama" value="{{ $cast->nama }}" required>

        <label for="bio">Deskripsi:</label>
        <textarea name="bio" required>{{ $cast->bio }}</textarea>

        <button type="submit">Simpan Perubahan</button>
    </form>
@endsection